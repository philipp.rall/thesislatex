\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{peasec}[2022/01/21 Typographic style for peasec style]

\def\@classname{peasec}

\LoadClass[a4paper]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% packages
\RequirePackage{ifthen}
\RequirePackage{kvoptions}

\RequirePackage{graphicx}
\RequirePackage[table,xcdraw]{xcolor}
\RequirePackage{setspace}
\RequirePackage{makecell}
\RequirePackage[skip=10pt]{parskip}

\RequirePackage{microtype}
\RequirePackage{textcase}
\RequirePackage{fontenc}

\usepackage[a-1b]{pdfx}

\RequirePackage{hyperref}
\RequirePackage{titlesec}
\RequirePackage{tocloft}

\RequirePackage{ifpdf}
\RequirePackage{ifxetex}
\RequirePackage{ifluatex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% kvoptions
\SetupKeyvalOptions{family=peasec, prefix=peasec@}

\DeclareBoolOption{red}
\DeclareBoolOption{blue}

\DeclareBoolOption{draft}
\DeclareBoolOption{print}
\DeclareBoolOption{thesis}
\DeclareBoolOption{german}
\DeclareBoolOption{anonymous}

\DeclareBoolOption{extendtoc}

\ProcessKeyvalOptions*


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% layout
\ifthenelse{\boolean{peasec@draft}}{
    \setlength\overfullrule{5pt}}{\relax}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% language
\newcommand{\if@german}[2]{\ifthenelse{\boolean{peasec@german}}{#1}{#2}}
\if@german{\RequirePackage[ngerman]{babel}}{\relax}

\if@german{\gdef\acksname{Danksagung}}{\gdef\acksname{Acknowledgements}}
\if@german{\gdef\bibname{Literatur}}{\gdef\bibname{Bibliography}}
\if@german{\gdef\appendixname{Anhang}}{\gdef\appendixname{Appendix}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
\ifthenelse{\boolean{peasec@print}}
    {\relax}
    {\hypersetup{
     colorlinks,
     linkcolor=black,
     filecolor=black,
     citecolor=black,
     urlcolor=black,
     linktoc=page,
     }
    }

\hypersetup{
	colorlinks,
	citecolor=black,
	filecolor=black,
	linkcolor=black,
	urlcolor=black
	}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% colors
\definecolor{fg}{RGB}{0,0,0}
\definecolor{bg}{RGB}{0,0,0}

\ifthenelse{\boolean{peasec@blue}}{%
    \definecolor{fg}{RGB}{15,112,183}
    \definecolor{bg}{RGB}{206,41,36}
    }{\relax}

\ifthenelse{\boolean{peasec@red}}{%
    \definecolor{fg}{RGB}{206,41,36}
    \definecolor{bg}{RGB}{15,112,183}
    }{\relax}

\definecolor{url}{named}{bg}
\definecolor{link}{named}{bg}
\definecolor{citation}{RGB}{0,0.5,0}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% fonts
\PassOptionsToPackage{T1}{fontenc}
\ifthenelse{\boolean{xetex}\OR\boolean{luatex}}
   {
    \RequirePackage{fontspec}
    \setmainfont[Ligatures=TeX,Numbers=OldStyle]{TeX Gyre Pagella}
    \linespread{1.05}
    \RequirePackage{unicode-math}
    \setmathfont{TeX Gyre Pagella Math}
    \setmonofont[Scale=0.85]{DejaVu Sans Mono}
   }
   {
    \RequirePackage{mathpazo}
    \linespread{1.05}
    \PassOptionsToPackage{scaled=0.85}{beramono}
    \RequirePackage{beramono}
   }

\newcommand\HUGE{\@setfontsize\Huge{50}{60}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sectioning appearance (titlesec)
\titleformat{\part}
    {\large} % format
    {\color{fg}\thepart} % label
    {1em} % sep
    {\spacedallcaps} % before code
    [\vspace{.9\baselineskip}] % after code

\titleformat{\section}
    {\relax} % format
    {\color{fg}\textsc{\thesection}} % label
    {1em} % sep
    {\spacedallcaps} % before code
    [\vspace{.9\baselineskip}] % after code


\titleformat{\subsection}
    {\relax} % format
    {\color{fg}\spacedallcaps{\thesubsection}} % label
    {1em} % sep
    {\normalsize\itshape} % before code
    [] % after code

\titleformat{\subsubsection}
    {\relax} % format
    {\color{fg}\spacedallcaps{\thesubsubsection}} % label
    {1em} % sep
    {\normalsize\itshape} % before code
    [] % after code

\titleformat{\paragraph}[runin]
    {\normalfont\normalsize} % format
    {\theparagraph} % label
    {0pt} % sep
    {\spacedlowsmallcaps} % before code
    [.] % after code

\titlespacing*{\section}{0pt}{2\baselineskip}{.5\baselineskip}
\titlespacing*{\subsection}{0pt}{1.25\baselineskip}{1\baselineskip}
\titlespacing*{\paragraph}{0pt}{1\baselineskip}{1\baselineskip}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% redefine ToC appearance
\PassOptionsToPackage{titles}{tocloft}

\setlength{\cftbeforepartskip}{1.5em}
\setlength{\cftbeforesecskip}{.5em}
\setlength{\cftbeforesubsecskip}{.1em}

\renewcommand{\cftpartfont}{\color{fg}\spacedallcaps\normalsize}
\renewcommand{\cftpartnumwidth}{1.5em}
\renewcommand{\cftpartindent}{0pt}
\renewcommand{\cftpartpresnum}{\color{fg}\scshape}
\renewcommand{\cftpartaftersnum}{\spacedlowsmallcaps}
\renewcommand{\cftpartpagefont}{\color{fg}\normalfont\normalsize}
\renewcommand{\cftpartafterpnum}{\normalfont\normalsize}

\renewcommand{\cftsecfont}{\normalfont\normalsize}
\renewcommand{\cftsecnumwidth}{1.5em}
\renewcommand{\cftsecindent}{0pt}
\renewcommand{\cftsecpresnum}{\color{fg}\scshape}
\renewcommand{\cftsecaftersnum}{\spacedlowsmallcaps}
\renewcommand{\cftsecpagefont}{\normalfont\normalsize}
\renewcommand{\cftsecafterpnum}{\normalfont\normalsize}

\setlength{\cftbeforesubsecskip}{0pt}
\renewcommand{\cftsubsecfont}{\normalfont\normalsize}
\renewcommand{\cftsubsecnumwidth}{3em}
\renewcommand{\cftsubsecindent}{1.5em}
\renewcommand{\cftsubsecpresnum}{\color{fg}\normalfont}
\renewcommand{\cftsubsecaftersnum}{\normalfont}
\renewcommand{\cftsubsecpagefont}{\normalfont\normalsize}
\renewcommand{\cftsubsecafterpnum}{\normalfont\normalsize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% new commands
\newcommand{\titlefontstyle}[1]{\Large\textls[20]{\scshape#1}}
\DeclareRobustCommand{\spacedallcaps}[1]{\scshape\textls[160]{\MakeTextUppercase{#1}}}
\DeclareRobustCommand{\spacedlowsmallcaps}[1]{\scshape\textls[80]{\MakeTextLowercase{#1}}}

\newcommand{\declarationname}{Ehrenwörtliche Erklärung}
\newcommand{\makedeclaration}[1]{
\section*{\declarationname}

Hiermit wird versichert von #1 die vorliegende \documenttype{} gemäß \S 22~Abs.~7 APB der TU Darmstadt ohne Hilfe Dritter und nur mit den angegebenen Quellen und Hilfsmitteln angefertigt zu haben. Alle Stellen, die Quellen entnommen wurden, sind als solche kenntlich gemacht worden. Diese Arbeit hat in gleicher oder ähnlicher Form noch keiner Prüfungsbehörde vorgelegen.
Mir ist bekannt, dass im Falle eines Plagiats (\S 38 Abs.~2 ~APB) ein Täuschungsversuch vorliegt, der dazu führt, dass die Arbeit mit 5,0 bewertet und damit ein Prüfungsversuch verbraucht wird. Abschlussarbeiten dürfen nur einmal wiederholt werden.

\bigskip

\noindent\textit{\location, \submissiondate} \hspace{3em} \hrulefill \\
\hspace*{0mm}\phantom{\textit{\location, \submissiondate} \hspace{3em}}  \centering#1

\ifthenelse{\boolean{peasec@extendtoc}}
    {\addcontentsline{toc}{section}{\declarationname}}
    {\relax}
}

\newcommand\frontmatter{%
    \cleardoublepage
  \pagenumbering{Roman}}

\newcommand\mainmatter{%
    \cleardoublepage
  \pagenumbering{arabic}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% template command and template values

\newcommand{\ifndef@gerEng}[3]{%
  \ifx#1\undefined
    \if@german{%
      \def#1{#2}
    }{%
      \def#1{#3}
    }
  \fi
}


\if@german{%
  \def\templatetitle{Exposé- und Thesis-Vorlage}
  \def\templateauthor{%
    \textbf{Vorname~Nachname,~B.~Sc.}\\
    \emph{Matr.-Nr.~1234567}\\
    \emph{\texttt{email@stud.tu-darmstadt.de}}}
}{%
  \def\templatetitle{Exposé and Thesis Template}
  \def\templateauthor{
    \textbf{Forename~Surname,~B.~Sc.}\\
    \emph{Matr.-Nb.~1234567}\\
    \emph{\texttt{mail@stud.tu-darmstadt.de}}}
}

\ifndef@gerEng{\university}{Technische Universität Darmstadt}{Technical University of Darmstadt}
\ifndef@gerEng{\department}{Informatik}{Computer Science}
\ifndef@gerEng{\field}{Wissenschaft und Technik für Frieden und Sicherheit~(PEASEC)}{Science and Technology for Peace and Security~(PEASEC)}

\ifndef@gerEng{\documenttype}{MasterThesis}{MasterThesis}
\ifndef@gerEng{\supervisor}{Betreuerin, Titel}{Supervisor, title}
\ifndef@gerEng{\examiner}{Prof.~Dr.~Dr.~Christian Reuter}{Prof.~Dr.~Dr.~Christian Reuter}

\ifndef@gerEng{\location}{Darmstadt}{Darmstadt}
\ifndef@gerEng{\submissiondate}{\today}{\today}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% renewed commands
\renewcommand{\maketitle}{%
    \begin{titlepage}
    \begin{center}
        \includegraphics[height=2cm]{figures/titlepage/tudalogo.pdf} \par
        \vspace{4em}

        \onehalfspacing
        {\color{fg}\titlefontstyle{\@title}} \par
        \singlespacing
        \vfill


        \begingroup
        \ifthenelse{\boolean{peasec@thesis}}{
        \if@german{%
            Am Fachbereich \department\\
            der Technischen Universität Darmstadt\\
            eingereichte\\\vspace{1em}
            {\spacedallcaps{\documenttype\ifdef{\creditpoints}{~(\creditpoints~CP)}{\relax}}} \\
            \vspace{1em}
            zur Erlangung des akademischen Grades\\
            \emph{\finaldegree}\\
            im \coursedegree{}-Studiengang \emph{\programme}
        }{%
            Submitted to the Department of \department\\
            of the \university\\\vspace{1em}
            {\spacedallcaps{\documenttype\ifdef{\creditpoints}{~(\creditpoints~CP)}{\relax}}} \\
            \vspace{1em}
            for the degree of\\
            \emph{\finaldegree}\\
            in the \coursedegree's programme \emph{\programme}
            \vspace{1em}
        	} \\
        \par\vspace{1em}}{\relax}
        \if@german{von}{by} \par
        \vspace{1em}
        {\begin{tabular}[t]{c}%
            \@author
         \end{tabular}} \par
        \endgroup

        \vfill

        \begin{minipage}{.6\textwidth}
            \begin{tabular}{rl}
                \ifdef{\supervisor}{\if@german{Prüfer}{Examiner}: & \makecell[tl]{\examiner} \\ }{\relax}
                \ifdef{\examiner}{\if@german{Betreuerin}{Supervisor}: & \makecell[tl]{\supervisor} \\ & \\}{\relax}
                \ifdef{\submissiondate}{\if@german{Tag der Einreichung}{Submitted}: & \submissiondate \\ }{\relax}
                \ifdef{\presentationdate}{\if@german{Präsentiert}{Presented}: & \presentationdate \\ }{\relax}
            \end{tabular}
        \end{minipage}

        \vspace{3em}

        {\includegraphics[height=2cm]{figures/titlepage/peaseclogo.png}}
    \end{center}
    \end{titlepage}
    \clearpage
}


\renewcommand{\@cftmaketoctitle}{
    \section*{\contentsname}
    \@starttoc{toc}
    \ifthenelse{\boolean{peasec@extendtoc}}
        {\addcontentsline{toc}{section}{\contentsname}}
        {\relax}
    }

\let\appendix@old=\appendix
\renewcommand{\appendix}{
    \appendix@old
    \ifthenelse{\boolean{peasec@thesis}}{\clearpage}{\relax}
    \addcontentsline{toc}{section}{\appendixname}
    \section*{\appendixname}
}

\renewcommand{\@cftmakelottitle}{
    \section*{\listtablename}
    \ifthenelse{\boolean{peasec@extendtoc}}
        {\addcontentsline{toc}{section}{\listtablename}}
        {\relax}
}

\renewcommand{\@cftmakeloftitle}{
    \section*{\listfigurename}
    \ifthenelse{\boolean{peasec@extendtoc}}
        {\addcontentsline{toc}{section}{\listfigurename}}
        {\relax}
}

\let\labelitemi@old=\labelitemi
\let\labelitemii@old=\labelitemii
\let\labelitemiii@old=\labelitemiii
\let\labelitemiv@old=\labelitemiv
\renewcommand{\labelitemi}{\color{fg}\labelitemi@old}
\renewcommand{\labelitemii}{\color{fg}\labelitemii@old}
\renewcommand{\labelitemiii}{\color{fg}\labelitemiii@old}
\renewcommand{\labelitemiv}{\color{fg}\labelitemiv@old}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% new environments
\newenvironment{acks}%
    {
     \ifthenelse{\boolean{peasec@thesis}}{\centering\begin{minipage}{.8\textwidth}}{\relax}
     \section*{\acksname}
     \ifthenelse{\boolean{peasec@extendtoc}}{\addcontentsline{toc}{section}{\acksname}}{\relax}
    }%
    {
     \ifthenelse{\boolean{peasec@thesis}}{\end{minipage}\clearpage}{\relax}
    }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% renewed environments
\renewenvironment{abstract}%
    {
     \ifthenelse{\boolean{peasec@thesis}}{\centering\begin{minipage}{.8\textwidth}}{\relax}
     \section*{\abstractname}
     \ifthenelse{\boolean{peasec@extendtoc}}{\addcontentsline{toc}{section}{\abstractname}}{\relax}
    }%
    {
     \ifthenelse{\boolean{peasec@thesis}}{\end{minipage}\clearpage}{\relax}
    }