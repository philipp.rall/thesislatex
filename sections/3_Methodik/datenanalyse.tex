\subsection{Datenanalyse}
\label{sec:datenanalyse}
Dieses Unterkapitel erläutert die Vorgehensweise in der Analyse der vorverarbeiteten Offline-und Online-Daten. Hierbei gilt es zwischen den beiden Unterforschungsfragen zu unterscheiden. \autoref{subsubsec:methodik:onlineOfflineVerbreitung} befasst sich mit der Datenanalyse für die erste Unterforschungsfrage \enquote{Wie verhält sich die geographische Verbreitung des Online-Protests zur Verbreitung des Offline-Protests?}. \autoref{subsubsec:methodik:ExistenzOffline} erläutert die Analyse zur Beantwortung der zweiten Unterforschungsfrage \enquote{Welche Eigenschaften von Online-Protest stehen im Zusammenhang mit der Existenz von Offline-Protest je Universität?}.

\subsubsection{Vergleich der Verbreitung von Online- und Offline-Protest }
\label{subsubsec:methodik:onlineOfflineVerbreitung}
Wie in \autoref{subsubsec:VerhaltenOnlineOffline} erläutert, lassen sich aus der Literatur drei mögliche Verhältnisformen von Online- und Offline-Protest herausarbeiten: ein negatives, positives oder reziprokes Verhältnis. Ziel der Analyse der ersten Unterforschungsfrage soll die Untersuchung der Verhältnisform der aufbereiteten Protestdaten sein. Hierzu lassen sich explorativ aus den Daten und aus der Literatur zwei potentielle Indikatoren ableiten: der Startzeitpunkt und die Protestausprägung des Online-Protests. Zu beiden Indikatoren sollen Thesen aufgestellt werden, die in der Analyse auf empirische Evidenz zu überprüfen sind. Die Wahl der beiden Indikatoren und die aufgestellten Thesen werden im Folgenden hergeleitet und begründet. \pagebreak

\textsc{\textbf{Indikator 1: Startzeitpunkt}}\\ 
Startet Online-Protest vor Offline-Protest, so hat Online-Protest potentiell einen Einfluss auf Offline-Protest und es lässt sich ein positives Verhältnis beider Protestformen vermuten. Starten beide zum ähnlichen Zeitpunkt, liegt eher ein reziprokes Verhältnis mit einem wie von \citet{hannaConceptualizingSocialProtest2016} beschriebenen real-digitalem Kreislauf vor. Deshalb soll das Verhältnis der Startzeitpunkte von Online- und Offline-Protest als Indikator für das Verhältnis der beiden Protestformen herangezogen werden. Ein negatives Verhältnis lässt sich durch diese Kenngrößen nicht abschätzen. Die bremsende Wirkung von Online-Protest auf Offline-Protest kann nicht durch einen früheren Startzeitpunkt von Offline-Protest gemessen werden.\\Der Startzeitpunkt von Offline-Protest ergibt sich aus dem ersten Protesttag im Beobachtungszeitraum. Bei Online-Protest kann zur Ermittlung des Startzeitpunktes nicht der Zeitpunkt des ersten Tweets herangezogen werden, da einzelne Tweets nicht den Start eines signifikanten Protestgeschehens anzeigen. Bei 85\% der Cluster liegt zudem der erste Tweet in den ersten drei Beobachtungstagen. Der Startzeitpunkt von Online-Protest charakterisiert sich empirisch vor allem durch eine überdurchschnittlich hohe Anzahl an Tweets innerhalb eines kurzen Zeitraums. Dadurch steigt die Relevanz und Tweets werden vom Algorithmus potentiell höher in der Timeline platziert, wodurch sie wiederum mehr Menschen erreichen und Online-Protest sich weiterverbreitet. Um diesen Zeitpunkt zu ermitteln werden alle Tweets eines Clusters je Tageshälfte aggregiert, d.h. jede Tageshälfte enthält die Summe aller Tweets dieser 12 Stunden. Als Start des Online-Protests wird die Tageshälfte gewählt, die am frühesten 25\% der Tweets der Tageshälfte mit der maximalen Anzahl an Tweets aufweist. Es soll folgende These untersucht werden, um ein positives Verhältnis der beiden Protestformen zu prüfen.
\begin{thesis}[1.1]
	Online-Protest startet im Mittel vor Offline-Protest.
\end{thesis}
Zur Überprüfung dieser These soll errechnet werden, wie viele Tage vor oder nach Offline-Protest Online-Protest im arithmetischen Mittel startet. Startet Online-Protest durchschnittlich vor Offline-Protest, so kann die These angenommen werden.

Außerdem soll allgemein untersucht werden, ob die Startzeitpunkte beider Protestformen nachweislich zusammenhängen. 
\begin{thesis}[1.2]
	Die Startzeitpunkte von Online- und Offline-Protest weisen einen starken Zusammenhang auf.
\end{thesis}
Der Zusammenhang beiden Startzeitpunkte soll durch die Bildung des Korrelationskoeffizienten nach Pearson $r_i$ bei Indikator-Eigenschaften $i=1$ quantifiziert werden. Zur Prüfung der Signifikanz wird ein zweiseitiger t-Test bei einem Signifikanzniveau von $\alpha=0,01$ angewandt. Bei einem signifikanten Ergebnis gilt die These als verifiziert.
\begin{equation}
	\label{eq:hypothese}
	\begin{aligned}
		\text{Nullhypothese:} \quad & H_0: \, r_i = 0 \\
		\text{Alternativhypothese:} \quad & H_1: \, r_i \neq 0
	\end{aligned}
\end{equation}
Ein zweiseitiger t-Test wird gewählt, da die Alternativhypothese ungerichtet ist. 

\textsc{\textbf{Indikator 2: Online-Protestausprägung}}\\
Liegt ein positives Verhältnis zwischen Online- und Offline-Protest vor, findet am Tag vor Offline-Protest überdurchschnittlich viel Online-Protest statt wie beispielsweise \citet{tanAnalyzingImpactSocial2013} herausarbeiten. Die Menge an Online-Protest lässt sich anhand der Anzahl an Tweets messen. \citet{tanAnalyzingImpactSocial2013} empfehlen das Tweet-Volumen als Indikator für auftretenden Offline-Protest. Es gilt nachfolgende These zu untersuchen. Mit vergleichbaren Tagen sind Tage mit gleicher Protest-Existenz gemeint, die dadurch vergleichbar sind. Im Gegensatz zu Indikator 1 werden hierfür alle Protestereignissen und nicht nur die Startzeitpunkte herangezogen, um ein vollständiges Bild zu erhalten.
\begin{thesis}[2.1]
	Am Tag vor Offline-Protest gibt es mehr Tweets als im Mittel vergleichbarer Tage.
\end{thesis}

Beeinflussen sich beide Protestformen positiv, so lässt sich aufbauend auf \citet{soler-i-martiInterdependencyOnlineOffline2020} auf ein reziprokes Verhältnis schließen. Dies soll im Folgenden anhand des Vergleichs der Online-Protestausprägung an Tagen mit Offline-Protest und an Tagen ohne Offline-Protest messbar gemacht werden. Online-Protestausprägung wird erneut durch die Anzahl an Tweets gemessen. Liegt an Tagen mit Offline-Protest im Mittel mehr Online-Protest als an Tagen ohne Offline-Protest vor, so kann eine positive gegenseitige Beeinflussung geschlussfolgert werden. Beide Protestformen treten am selben Tag auf. Die Vergleichsgröße des Mittelwerts an Tweets aller Tage ohne Offline-Protest wird je Cluster gebildet werden, da die Anzahl an Tweets je Cluster stark schwankt und nicht zwischen den Clustern vergleichbar ist.
\begin{thesis}[2.2]
	An Tagen mit Offline-Protest gibt es im Mittel mehr Tweets als an Tagen ohne Offline-Protest.
\end{thesis}
Zur Überprüfung der beiden Thesen von Indikator 2 sollen zwei Kennzahlen definiert werden, die Protest-Positivitätszahl $\psi$ für These 2.1 und die Protest-Reziprozitätszahl $\rho$ für These 2.2. Der folgende Abschnitt erläutert die Definition beider Kennzahlen.\\
Um bei Überprüfung von These 2.1 reziproke Effekte zu verringern, ist es wichtig, als Vergleichswert den Mittelwert ähnlicher Tage heranzuziehen, d.h. von Tagen mit gleicher Protest-Existenz. Wenn am Vortag ebenfalls Offline-Protest existiert, so wird deshalb der Mittelwert aller Tage mit Offline-Protest als Vergleich herangezogen. Liegt kein Offline-Protest vor, dient der Mittelwert aller Tage ohne Offline-Protest als Vergleichsgröße. So kann die mögliche Beeinflussung durch den Offline-Protest reduziert werden.\\ Für die nachfolgenden Definitionen soll folgende Notation eingeführt werden:
\begin{alignat*}{2}
	&C &\quad &\text{Menge an Clustern, es gilt }C= C^P \cup C^{\neg P} \\
	&C^P &&\text{Menge an Clustern mit Offline-Protesten} \\
	&C^{\neg P} &&\text{Menge an Clustern ohne Offline-Protesten} \\
	&k=|C^P| &&\text{Anzahl an Clustern mit Offline-Protest} \\
	&c \in C &&\text{einzelnes Cluster} \\
	&n_c \in \mathbb{N}&&\text{Anzahl an Tagen mit Offline-Protest des Clusters }c\\
	&n_c^A \in \mathbb{N}&&\text{Anzahl an Tagen mit Offline-Protest des Clusters }c\text{,}\\
	&&&\text{für die die Aussage }A\text{ zutrifft}
\end{alignat*}
\label{eq:Formelzeichen}
Die Protest-Positivitätszahl $\psi$ ist wie folgt definiert:
\begin{equation}
	\label{eq:positivität}
	\text{Protest-Positivitätszahl }\psi \coloneq \frac{\sum_{c=1}^{k} n^A_c}{\sum_{c=1}^{k} n_c}
\end{equation}
Hierbei sei Aussage $A=$\textit{„Findet am Vortag Offline-Protest statt, liegt die Tagesanzahl an Tweets des Vortags über} $m_c^P$\textit{. Findet am Vortag kein Offline-Protest statt, liegt die Tagesanzahl an Tweets des Vortags über} $m_c^{\neg P}$\textit{“}. $m_c^P$ sei das arithmetische Mittel der Anzahl an Tweets von Tagen mit Offline-Protest eines Clusters $c$, $m_c^{\neg P}$ sei das arithmetische Mittel der Anzahl an Tweets von Tagen ohne Offline-Protest eines Clusters $c$. Im Zähler von Formel \ref{eq:positivität} steht folglich die Summe der Protesttage aller Cluster, auf die $A$ zutrifft, im Nenner steht die Summe der Protesttage aller Cluster.

Die Überprüfung von These 2.2 erfolgt durch die Protest-Reziprozitätszahl $\rho$, die wie folgt definiert ist:
\begin{equation}
	\label{eq:reziprozität}
	\text{Protest-Reziprozitätszahl }\rho \coloneq \frac{\sum_{c=1}^{k} n^B_c}{ \sum_{c=1}^{k}n_c}
\end{equation}
Hierbei sei Aussage $B=$\textit{„Die Tagesanzahl an Tweets liegt über} $m_c^{\neg P}$ \textit{“}. $m_c^{\neg P} $ sei das arithmetische Mittel der Anzahl an Tweets von Tagen ohne Offline-Protest eines Clusters $c$. 

Zur Interpretation der Protest-Positivitäts- und Protest-Reziprozitätszahl können die Intervalle in \autoref{tab:kennzahlen} eine Orientierung bilden. Liegt der Anteil an Tagen mit der jeweiligen Eigenschaft unter oder bei 50\% so kann nicht von einem aussagekräftigen, allgemeingültigen Zusammenhang gesprochen werden. Bei beobachtetem Auftreten des Verhältnisses handelt sich nicht um die Mehrheit der entsprechenden Tage, sonder um Einzelfälle. Liegt der Anteil über 50\% gilt die jeweilige These als verifiziert. Je höher der Anteil wird, desto allgemeingültiger wird der gemessene Zusammenhang beider Protestformen.
\begin{table}[h!]
	\centering
	\begin{tabular}{|
			>{\columncolor[HTML]{EFEEEE}}c |cc|}
		\hline
		\multicolumn{1}{|l|}{\cellcolor[HTML]{EFEEEE}\textbf{}} & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEEEE}\textbf{Protest-Positivitätszahl $\psi$}} & \cellcolor[HTML]{EFEEEE}\textbf{Protest-Reziprozitätszahl $\rho$} \\ \hline
		\textbf{$[0; 0,5]$}                                       & \multicolumn{2}{c|}{kein aussagekräftiger Zusammenhang}                                                                                                   \\ \hline
		\textbf{$(0,5; 0,75]$}                                    & \multicolumn{1}{c|}{schwach positives Verhältnis}                                     & schwach reziprokes Verhältnis                                     \\ \hline
		\textbf{$(0,75; 1]$}                                      & \multicolumn{1}{c|}{stark positives Verhältnis}                                       & stark reziprokes Verhältnis                                       \\ \hline
	\end{tabular}
	\caption[Interpretation der Protest-Positivitäts- und -Reziprozitätszahl]{Interpretation der Protest-Positivitäts- und Protest-Reziprozitätszahl}
	\label{tab:kennzahlen}
\end{table}

\subsubsection{Indikatoren von Online-Protest für Existenz von Offline-Protest}
\label{subsubsec:methodik:ExistenzOffline}
Ziel der Analyse der zweiten Unterforschungsfrage soll die Ermittlung von aussagekräftigen Indikator-Eigenschaften von Online-Protest für die Existenz von Offline-Protest sein. Hierzu lassen sich explorativ aus den Daten und aus der Literatur vier potentielle Indikatoren ableiten: der Anteil an Metavoicing-Tweets, der Anteil involvierter Studierender, die Unterstützung aus dem Ausland und die Anzahl an Tweets pro User. Zu jedem Indikator sollen Thesen aufgestellt werden, die einen Zusammenhang zwischen der Indikator-Eigenschaft des Online-Protests und der Existenz von Offline-Protest vermuten. Im Folgenden wird die Wahl der Indikatoren begründet und werden die aufgestellten Thesen hergeleitet. Anschließend wird für alle Indikatoren die Vorgehensweise zur Überprüfung der aufgestellten Thesen erläutert.

\textsc{\textbf{Indikator 3: Metavoicing-Tweets}}\\
Nach der Mobilisierungstheorie von \citet{ruchtBedeutungOnlineMobilisierungFuer2014} und \citet{vanstekelenburgSocialPsychologyProtest2013} ist für die Mobilisierung besonders wichtig, viele Menschen zu erreichen, um sie zunächst vom Protestanliegen zu überzeugen (Konsensusmobilisierung) sowie anschließend über Protesttermine zu informieren und zum Protestieren zu motivieren (Aktionsmobilisierung).\\
Der Grad an Weiterverbreitung lässt sich bei Online-Protest anhand des Anteils an Metavoicing-Tweets nach der Unterteilung von \citet{georgeClicktivismHacktivismUnderstanding2019a} messen. Sie differenzieren die Ausdrucksformen digitalen Protests wie in \autoref{subsubsec:onlineProtest} beschrieben in drei Hierarchiestufen. In der ersten Hierarchiestufe, den \textit{Spectator Activities}, unterscheiden die Autor*innen zwischen \textit{Clicktivism}, \textit{Metavoicing} und \textit{Assertion}. Unter \textit{Assertion} verstehen die Autor*innen selbst erstellte Beiträge - auf Twitter bezogen originale Tweets. Unter \textit{Metavoicing}-Aktivitäten verstehen sie Beiträge, die Nutzende nicht selbst erstellt haben, sondern teilen oder kommentieren. Darunter fallen bei Twitter \textit{Retweets}, \textit{Quotes} und \textit{Replies}, die diese Arbeit als Metavoicing-Tweets zusammengefasst werden.\\
Je stärker Online-Protest weiterverbreitet wird, desto besser gelingt potentiell die Mobilisierung für Offline-Protest. Daher lässt sich folgende These aufstellen:
\begin{thesis}[3]
	Der Anteil von Metavoicing-Tweets im Online-Protest ist ein Indikator für die Existenz von Offline-Protest.
\end{thesis}

\textsc{\textbf{Indikator 4: Involvierte Studierende}}\\
Je mehr Studierende einer Universität an Online-Protest beteiligt sind, desto stärker ist die Präsenz eines Themas in der Studierendenschaft und desto einfacher gelingt beispielsweise die für die Aktionsmobilisierung wichtige Informationsvermittlung von geplanten Protestterminen. 
Bei einem höheren Anteil involvierter Studierender ist es wahrscheinlicher, dass unbeteiligte Studierende bereits Personen, die sich am Protestgeschehen beteiligen, in ihrem Freundes- oder Bekanntenkreis haben. Diese soziale Einbettung erleichtert nach \citet{vanstekelenburgSocialPsychologyProtest2013} den Einstieg in den Protest und kann ein Auslöser für Protestpartizipation sein.\\
Um den Anteil an involvierten Studierenden aus der Anzahl an am Online-Protest beteiligten Usern zu konstruieren, ist eine Grundgesamtheit aller Studierender erforderlich. Absolut lassen sich die User-Zahlen zwischen den Clustern nicht vergleichen, da die Grundgesamtheit stark variiert. Da die demographische Grundgesamtheit an in einem Cluster lebenden Menschen nicht bekannt ist, soll diese anhand der Anzahl an Studierenden der Universität(en) des Clusters aus dem Jahr 2015 angenähert werden \citep{staffwriterHereAreSouth2015}. Liegen mehrere Universitäten im 25 \si{\km}-Radius, so gilt die Summe deren Studierender als \linebreak approximative Grundgesamtheit.
\begin{equation*}
	\textrm{Anteil involvierter Studierender} \coloneqq \frac{\textrm{Anzahl an Usern im Online-Protest}}{\textrm{Anzahl an Studierenden}}
\end{equation*}
Hieraus ergibt sich die folgende These: 
\begin{thesis}[4]
	Der Anteil an involvierten Usern an der Anzahl aller Studierender ist ein Indikator für die Existenz von Offline-Protest.
\end{thesis}

\textsc{\textbf{Indikator 5: Unterstützung aus dem Ausland}}\\
Teilweise beeinflussen User, die aus der Diaspora den Online-Protest unterstützen, auch das Protestgeschehen im Land (\citet{belloEndSARSProtestDiscourse2023}, \citet{olabodeVeteransDiasporaActivism2016},  \citet{monteiroFarmerProtestsIndia2021}). Daher soll in diesem Indikator untersucht werden, ob der Anteil an Usern, die sich aus dem Ausland am Online-Protest beteiligen, im Zusammenhang mit der Existenz von Offline-Protesten steht. Ob ein User im Ausland oder Inland lebt, kann anhand der Standortangabe im Profil der User ermittelt werden.
\begin{thesis}[5]
	Der Anteil an im Online-Protest involvierten Usern aus dem Ausland ist ein Indikator für die Existenz von Offline-Protest.
\end{thesis}

\textsc{\textbf{Indikator 6: Anzahl an Tweets pro User}}\\
Wenn User stark im Online-Protest beteiligt sind, so besitzt die Thematik mutmaßlich eine hohe Bedeutung für sie. Deshalb kann vermutet werden, dass sie potentiell auch zur Teilnahme an Offline-Protest neigen. Die Beteiligung der User an Online-Protest soll anhand der im Mittel veröffentlichten Tweets je User gemessen werden.
Es lässt sich folgende These aufstellen:
\begin{thesis}[6]
	Die durchschnittliche Anzahl an Tweets je User ist ein Indikator für die Existenz von Offline-Protest.
\end{thesis}

Zur Überprüfung der vier aufgestellten Thesen werden jeweils drei Methoden angewandt, die im Folgenden vorgestellt werden.\pagebreak

Als \textbf{erste Methode} sollen je Indikator-Eigenschaft die Mittelwerte aller Cluster mit Offline-Protest und ohne Offline-Protest verglichen werden, um eine potentielle Gemeinsamkeit der Cluster ohne bzw. mit Offline-Protest zu erkennen. Weicht der Mittelwert aller Cluster mit Offline-Protest signifikant vom Mittelwert der Cluster ohne Offline-Protest ab, so unterstützt dies die Verifizierung der These. Eine Abweichung sei signifikant, sofern sie größer als 25\% ist.

Die \textbf{zweite Methode} bestimmt Schwellwerte von Indikator-Eigenschaften, für die aussagekräftige Zusammenhänge zwischen Indikator und Protest-Existenz bestehen. Die Schwellwerte sind so zu wählen, dass zum einen der Schwellwert möglichst niedrig ist. Zum anderen soll gleichzeitig die Wahrscheinlichkeit für Offline-Protest in einem Cluster, kurz Protestwahrscheinlichkeit, möglichst hoch sein, wenn die Indikator-Eigenschaft den Schwellwert überschreitet. Für die nachfolgenden Definitionen sollen zusätzlich zu der bereits eingeführten Notation von \autopageref{eq:Formelzeichen} folgende Bezeichnungen verwendet werden:
\begin{alignat*}{2}
	&i \in \mathbb{N}&\quad&\text{Indikator-Eigenschaft} \\
	&c(i) \in \mathbb{R}&&\text{Wert der Indikator-Eigenschaft }i\text{ in Cluster }c\text{, }c(i)\geq 0\\
	&\tau_i \in \mathbb{R} && \text{Schwellwert von Indikator-Eigenschaft }i\text{, }\tau_i \geq 0 
\end{alignat*}
Die Protestwahrscheinlichkeit $Q_i^{\tau_i}$ einer Indikator-Eigenschaft $i$ bei Überschreitung eines Schwellwerts $\tau_i $ sei wie folgt definiert:
\begin{equation}
	\label{eq:Schwellwert}
	Q_i^{\tau_i} \coloneq \frac{|\{c \in C^P|c(i)>\tau_i\}|}{|\{c \in C|c(i)>\tau_i\}|}
\end{equation}
Die Formel beschreibt das Verhältnis der Anzahl an Clustern mit Offline-Protest über dem Schwellwert zu der Anzahl aller Cluster über dem Schwellwert.

Die Aussagekraft der ermittelten Schwellwerte $S_i^{\tau_i}$ kann anhand des Anteils an Clustern über dem Schwellwert an allen Clustern ermittelt werden. 
\begin{equation}
	\label{eq:AussagekraftSchwellwert}
	S_i^{\tau_i} \coloneq  \frac{|\{c \in C|c(i)>\tau_i\}|}{|C|}
\end{equation}
Ist dieser Anteil gering, so hat der Schwellwert selbst bei hoher Protestwahrscheinlichkeit eine geringe Aussagekraft, da er nur für einen geringen Anteil aller Cluster zutrifft. Die Signifikanz des Schwellwerts soll anhand einer Referenz-Protestbeteiligung $Q_i^0$ bei einem Schwellwert von $\tau_i=0$ definiert werden. Bei einem Schwellwert von $\tau_i=0$ liegen alle Cluster über oder auf dem Schwellwert, da Indikator-Eigenschaften per Definition keine negativen Werte annehmen können. Somit gilt: Wenn $Q_i^{\tau_i}$ über der Referenz-Protestbeteiligung $Q_i^0$ liegt und $S_i^{\tau_i}$ gleichzeitig signifikant hoch ist, so unterstützt dies die Verifizierung der These. Als signifikante Höhe von $S_i^{\tau_i}$ sei ein Wert größer als 75\% definiert.

Als \textbf{dritte Methode} sollen die Zusammenhänge zwischen den Indikator-\linebreak Eigenschaften und der Existenz von Offline-Protest durch einen Korrelationskoeffizienten quantifiziert werden. Für die Existenz von Protesten wird die binäre Variable  $Protestexistenz$ eingeführt, die genau dann für ein Cluster $c$ den Wert $1$ hat, wenn mindestens ein Offline-Protest in diesem Cluster $c$ existiert. Ansonsten weist sie den Wert $0$ auf. Für die Korrelation mit der Variablen $Protestexistenz$ soll punktbiseriale Korrelation statt der Korrelation nach Pearson verwendet werden, da letztere für jeweils intervallskalierte Variablen konzipiert ist. Punktbiseriale Korrelation hingegen ist für eine binäre und eine intervallskalierte Variable definiert \citep{kornbrotPointBiserialCorrelation2014}. 
Zur Prüfung der Signifikanz wird wie in \autoref{subsubsec:methodik:onlineOfflineVerbreitung} ein zweiseitiger t-Test bei einem Signifikanzniveau von $\alpha=0,01$ angewandt. Dabei soll folgende Hypothese bei punktbiserialem Korrelationskoeffizienten $p_i$ und Indikator-Eigenschaft $i$ überprüft werden:
\begin{equation}
	\label{eq:hypothese2}
	\begin{aligned}
		\text{Nullhypothese:} \quad & H_0: \, p_i = 0 \\
		\text{Alternativhypothese:} \quad & H_1: \, p_i \neq 0
	\end{aligned}
\end{equation}
Wie in \autoref{subsubsec:methodik:onlineOfflineVerbreitung} wird ein zweiseitiger t-Test gewählt, da die Alternativhypothese ungerichtet ist. Liegt eine signifikante Korrelation vor, so unterstützt dies die Verifizierung der These.

Die ersten beiden Methoden können auch nicht-lineare Zusammenhänge zwischen der jeweiligen Indikator-Eigenschaft und der Existenz von Protest erkennen. Methode 1 untersucht allgemein Gemeinsamkeiten zwischen Clustern mit und ohne Protest. Methode 2 nutzt eine Rangordnung und berücksichtigt die Anzahl der Datenpunkte, die einen Schwellwert überschreiten, statt der eigentlichen Werte. Die dritte Methode, der Korrelationskoeffizient, ist dahingegen ein spezifisches Maß für eine mögliche lineare Beziehung zwischen der jeweiligen Indikator-Eigenschaft und der Existenz von Protest. Deshalb gilt zusammenfassend, dass für eine Verifizierung einer These mindestens die ersten beiden Methoden oder der Korrelationskoeffizient ein signifikantes Ergebnis erzielen müssen. Es können auch alle drei Methoden signifikante Ergebnisse liefern.