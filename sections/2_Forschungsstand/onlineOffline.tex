\subsection{Zusammenspiel Online- und Offline-Protest}
\label{sec:onlineOffline}
Das folgende Unterkapitel gibt einen Überblick über die Literatur zu Online- und Offline-Protest, definiert beide Begriffe und geht auf deren Verhältnis ein.

\subsubsection{Offline-Protest}
Nach \citet{casquetePowerDemonstrations2006} lässt sich Protest als symbolische Darbietung einer Menge an Menschen im öffentlichen Raum mit dem Ziel, Entscheidungstragende, die öffentliche Meinung oder Teilnehmende zu beeinflussen, definieren.
\\In dieser Arbeit bezeichnet Offline-Protest dabei alle Protestformen, die im analogen öffentlichen Raum stattfinden, d.h. beispielsweise auf physischen Plätzen oder Straßen. Darunter fallen unter anderem Demonstrationen, Straßenblockaden oder Streiks (\cite{hannaConceptualizingSocialProtest2016}).

\subsubsection{Online-Protest}
\label{subsubsec:onlineProtest}
Unter Online-Protest versteht diese Arbeit alle Protestformen, die im digitalen öffentlichen Raum, d.h. im Internet stattfinden. Ein besonderer Fokus liegt hierbei auf der Plattform Twitter, auf die Abschnitt \ref{sec:twitter} näher eingeht.

Online-Protest verfolgt dabei nach \citet{veghClassifyingFormsOnline2003} drei wesentliche Ziele:
\begin{itemize}
	\item \textbf{Bewusstseinsbildung und Befürwortungsarbeit}: Verbreiten von Informationen wie persönliche Meinungen oder Erfahrungen, um ein kollektives Bewusstsein dafür zu schaffen (\cite{greijdanusPsychologyOnlineActivism2020}). Dadurch kann die in Abschnitt \ref{sec:ressourcenmobilisierung} erläuterte Konsensusmobilisierung erfolgen. %Literaturverweis passt nicht 100%
	\item \textbf{Organisation und Mobilisierung}: Aufruf zu und Koordination von Aktionen, hierbei handelt es sich um Aktionsmobilisierung.
	\item \textbf{Aktionen und Reaktionen}: Durchführung von Protestaktionen im digitalen Raum wie beispielsweise Online-Petitionen, Massen-Emails, Blockaden von Webseiten, Offenlegung von vertraulichen Daten oder Hacking (\cite{georgeClicktivismHacktivismUnderstanding2019a}).
\end{itemize}
%\todo{Literaturverweise passen nicht perfekt}
%FMF online Protest vor allem erste beide Ziele

\citet{georgeClicktivismHacktivismUnderstanding2019a} arbeiten in einer breiten Literaturrecherche zehn konkrete Ausdrucksformen von Online-Protest heraus, die basierend auf den drei Hierarchiestufen der politischen Partizipation von \citet{milbrathPoliticalParticipationHow1977} strukturiert sind (siehe \autoref{fig:hierachydigitalactivism}). Beginnend mit der niedrigsten Hierarchiestufe, den \textit{Spectator Activities}, sinkt über die  \textit{Transitional Activites} und \textit{Gladiatoral Activities} jeweils die Anzahl an Aktionen und Beteiligten und steigen die individuell investierten Ressourcen und direkten, potentiellen Auswirkungen.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\linewidth]{figures/2_Forschung/hierachyDigitalActivism}
	\caption[Hierarchie von digitalem Aktivismus]{Hierarchie von digitalem Aktivismus nach \citet{georgeClicktivismHacktivismUnderstanding2019a}, eigene Darstellung}
	\label{fig:hierachydigitalactivism}
\end{figure}
So fallen beispielsweise einfache Formen des Online-Protests wie \textit{Liking} oder \textit{Retweeting} in die erste Hierarchiestufe, Online-Petitionen in die zweite und Hacking in die dritte Hierarchiestufe. \\Besonders interessant für diese Arbeit sind die Ausdrucksformen digitalen Protests der ersten Hierarchiestufe, da diese vor allem auf Social Media Plattformen wie Twitter stattfinden (\cite{georgeClicktivismHacktivismUnderstanding2019a}).
Unter digitalen \textit{Spectator Activities} verstehen die Autor*innen:
\begin{enumerate}
	\item \textbf{Clicktivism}: Zustimmung zu bestehenden Beiträgen oder Profilen durch Likes oder Folgen
	\item \textbf{Metavoicing}: Verbreitung von Beiträgen durch Teilen, Retweeten oder Reaktion durch Kommentare
	\item \textbf{Assertion}: Erstellung von Beiträgen, aber auch Aktivitäten wie Versenden von E-Mails an Politiker*innen
\end{enumerate}

\subsubsection{Verhältnisformen Online- und Offline-Protest}
\label{subsubsec:VerhaltenOnlineOffline}
%Sorge zu nah an Grejdamus
In der Literatur lassen sich drei mögliche Verhältnisformen zwischen Online- und Offline-Protest ausmachen: ein negatives, positives oder reziprokes Verhältnis. 

Nur wenige Autor*innen argumentieren für ein negatives Verhältnis der beiden Protestformen, d.h. eine bremsende Wirkung von Online-Protest auf Offline-Protest. Dabei beziehen sie sich auf \textit{Slacktivism}, was kleine symbolische Beiträge zur Unterstützung einer Thematik mit geringem Aufwand und Risiko bezeichnet (\cite{piatSlacktivismNotSimply2019}). Beispiele für \textit{Slacktivism} sind das Unterschreiben einer Online-Petition oder Liken eines Beitrags. Die Auswirkungen von \textit{Slacktivism} auf die Bereitschaft für Offline-Protest sind wissenschaftlich umstritten. \\
\citet{schumannSubstituteSteppingStone2015} arbeiten heraus, dass \textit{Slacktivism} einen demobilisierenden Effekt habe. Durch Experimente belegen die Autor*innen die These, dass Nutzende von Social Media Online-Protest als ausreichenden Beitrag zu einem in einer Gruppe geteilten Ziel einschätzen würden und deshalb weniger gewillt für Offline-Protest seien. Auch  \citet{wilkinsAllClickNo2019} führen an, dass \textit{Slacktivism} zu geringerem zukünftigem Engagement und nicht zu sozialen Veränderungen führe. Andere Autor*innen hingegen verstehen \textit{Slacktivism} als \enquote{legitime und nützliche Form politischen Engagements} und argumentieren, dass auch viele kleine Beiträge in Summe offline Wirkung entfalten können \citep{piatSlacktivismNotSimply2019}.

Die Mehrheit der Veröffentlichungen plädiert für ein signifikantes positives Verhältnis von Online- und Offline-Protest und kommt zu dem Ergebnis, dass vorangegangener Online-Protest die Beteiligung an Offline-Protest erhöht (\cite{andersonNetworkedRevolutionsICTs2021}). \citet{tufekciSocialMediaDecision2012} können diesen Effekt durch Umfragen bei den Protesten am Tahrir Platz in Ägypten 2011 nachweisen. Zu diesem Fazit kommt auch die Arbeit von \citet{angyalHowAreOnline2020}, die 14 Studien auswertet und zusammenträgt. Davon nutzen zehn Studien Umfragen und Interviews, drei Paneldaten und eine ein Experiment als Forschungsmethode.\\ \citet{bastosTentsTweetsEvents2015} wählen einen weiteren Ansatz und untersuchen das Verhältnis von Online- und Offline-Protest durch den Test beider Zeitreihen auf Granger-Kausalität. Granger-Kausalität ist ein statistischer Test, der eine Aussage darüber trifft, ob eine Zeitreihe die Vorhersage einer anderen Zeitreihe signifikant verbessert (\cite{rottmannDefinitionGrangerKausalitaet}). Im Fallbeispiel der Vinegar-Proteste 2013 in Brasilien können sie eine Granger-Kausalität feststellen, was ebenfalls ein positives Verhältnis von Online- auf Offline-Protest vermuten lässt.

Neben dem positiven und negativen Verhältnis gibt es die Theorie des reziproken Verhältnisses von Online- und Offline-Protest, in dem sich beide Protestsphären gegenseitig unterstützen und positiv beeinflussen (\cite{soler-i-martiInterdependencyOnlineOffline2020}).\\ \citet{hannaConceptualizingSocialProtest2016} sprechen hierbei von einem real-digitalen Kreislauf mit einem Wechselspiel von Digitalisierung und Realisierung. Offline-Protest wird durch das Teilen von Bildern, Eindrücken und Erfahrungen im Internet digitalisiert, wodurch Online-Protest unterstützt wird. Online-Protest im Internet wird durch Mobilisierung für Offline-Proteste in die reale, analoge Welt getragen und realisiert, sodass Offline-Protest unterstützt wird. Digitalisieren Teilnehmende des Offline-Protests diesen erneut, ergibt sich ein Kreislauf und ein reziprokes Verhältnis von Online- und Offline-Protest. \autoref{fig:digitalisierungrealisierung} stellt diesen Kreislauf zwischen Online- und Offline-Protest dar. \pagebreak

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\linewidth]{figures/5_Ergebnisse/DigitalisierungRealisierung}
	\caption[Real-digitaler Kreislauf von Online- und Offline-Protest]{Real-digitaler Kreislauf nach \citet{hannaConceptualizingSocialProtest2016}, eigene Darstellung}
	\label{fig:digitalisierungrealisierung}
\end{figure}
Zusätzlich zu den drei angeführten Verhältnisformen plädieren \citet{vissersImpactMobilizationMedia2012} für die These, dass Mobilisierung medium-spezifisch sei und Online- und Offline-Protest getrennt betrachtet werden müssten. Sie arbeiten in ihrer Arbeit durch eine experimentelle Studie heraus, dass Online-Mobilisierung einen signifikanten Effekt auf Online-Protest und Offline-Mobilisierung eine signifikante Auswirkung auf Offline-Protest besitze.