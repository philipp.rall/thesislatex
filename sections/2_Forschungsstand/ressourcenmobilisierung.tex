Dieses Kapitel legt mit einem Überblick über den bisherigen Stand der Forschung zu Protest- und Mobilisierungstheorie in \autoref{sec:ressourcenmobilisierung}, dem Zusammenspiel von Online- und Offline-Protest in \autoref{sec:onlineOffline} und Twitter in \autoref{sec:twitter} die theoretischen Grundlagen dieser Arbeit. \autoref{sec:forschungslücke} arbeitet darauf aufbauend die Forschungslücke heraus, die diese Arbeit adressiert.
\subsection{Protest- und Mobilisierungstheorie}
\label{sec:ressourcenmobilisierung}
Dieses Unterkapitel führt in die für die Arbeit notwendige Protesttheorie und Theorie zu sozialen Bewegungen ein. Es soll Hintergründe zu den Fragen liefern, unter welchen Voraussetzungen Proteste entstehen und Menschen sich entscheiden, mitzuwirken. Außerdem legt das Unterkapitel Grundlagen in der Mobilisierungstheorie.

\subsubsection{Soziale Bewegungen}
Soziale Bewegungen lassen sich als \textit{\enquote{networks of individuals and groups, based on shared collective identities, engaged in political or social conflicts’}} definieren (\citet{rochonCultureMovesIdeas1998} zitiert nach \cite[208]{gillComparativeFrameworkAnalysis2009}). Studentische Bewegungen stellen eine Teilmenge von sozialen Bewegungen dar und zeichnen sich nach \citet{altbachPerspectivesStudentPolitical1989} und \citet{luescherFeesMustFallInternetAgeStudent2017} durch ihre fließenden Führungsstrukturen, kurze Dauer und ihre volatile Ausrichtung aus.
%Differenzierungen value-oriented vs. norm-oriented, etudalisitc vs. society-oriented mit reinnehmen?
%Typologie in vier Arten nach Gill mit reinnehmen?
\\\citet{castellsNetworksOutrageHope2015} führt zusätzlich die Kategorie der \textit{internet-age network movements} ein, die sich durch die Nutzung des virtuellen Raums charakterisieren lassen und dadurch eine breitere und diversere Gruppe an Menschen mobilisieren können. \citet{ntuliImplicationsSocialMedia2017} ordnen die \#FeesMustFall-Proteste in Südafrika in diese Kategorie ein.

\subsubsection{Auslöser zur Partizipation an Protest}
Es lässt sich allgemein zwischen individuellen und strukturellen Auslösern zur Teilnahme an bzw. Bildung von Protesten unterscheiden. Zunächst soll in diesem Abschnitt der Fokus auf individuelle Auslöser gelegt werden, die eine Einzelperson dazu bewegen, sich einem Protest anzuschließen.
In ihrer Publikation \citetitle{vanstekelenburgSocialPsychologyProtest2013} listen \citet{vanstekelenburgSocialPsychologyProtest2013} fünf individuelle Auslöser auf, die im Folgenden dargelegt werden und in der Praxis meist miteinander verwoben sind.
\begin{itemize}
	\item \textbf{Missstände}: Wahrgenommene Missstände und Ungerechtigkeiten sind die traditionelle Erklärung für Proteste \citep{jenkinsResourceMobilizationTheory1983}. Aber auch Konflikte und gewaltvolle Gegenreaktionen gegen Proteste können als Auslöser dienen (\cite{altbachStudentPoliticalActivism1991}, zitiert nach \cite{luescherStudentPoliticsAfrica2016})
	\item \textbf{Wirksamkeit}: Trotz vieler existierender Missstände weltweit, kommt es nicht zwangsläufig zu Protesten. Dies liegt nach \citet{vanstekelenburgSocialPsychologyProtest2013} daran, dass die Erwartung, dass durch den Protest eine Änderung bzw. Verbesserung erzielt werden kann, nicht immer gegeben sei. Das Gefühl von Wirksamkeit sei jedoch für eine Protest-Entscheidung fundamental.
	\item \textbf{Kollektive Identität}: Identifizieren sich Menschen mit einer Gruppe, so sind sie leichter gewillt für diese und deren Ziele zu protestieren. Eine Gruppenidentität schafft zudem ein Gefühl innerer Verpflichtung, auch für die Gruppe einzutreten, aktiv zu werden und am Protest teilzunehmen. Gleichzeitig schreibt \citet{melucci1988getting} zitiert nach \citet[446]{buechlerNewSocialMovement1995}, dass unsere schnelllebige Welt eine \enquote{Obdachlosigkeit der persönlichen Identität} schaffe, weshalb Menschen nach ebendiesen kollektiven Identitäten streben würden.
	\item \textbf{Emotionen}: Die dominante Emotion, die Entscheidungen zu Protest auslöst, ist Wut \citep{castellsNetworksOutrageHope2015}. Die Ursache der Emotion liegt meist in wahrgenommenen Missständen und Ungerechtigkeiten.
	\item \textbf{Soziale Einbettung}: Häufig trägt auch die Entscheidung nahestehender Menschen, sich an einem Protest zu beteiligen, zur eigenen Protestbeteiligung bei. 
\end{itemize}

Neben den individuellen Auslösern werden in der Literatur auch strukturelle Gründe für das Entstehen von studentischen Protestereignissen aufgeführt.\\
\citet{luescherStudentPoliticsAfrica2016} stellen die Frage, an welchen Universitäten studentischer Protest am wahrscheinlichsten entsteht. Nach ihrer Forschung basierend auf \citet{altbachStudentPoliticalActivism1991}, neigen allgemein betrachtet vor allem privilegierte Studierende an renommierten Universitäten mit kosmopolitischen Professor*innen zu Protest. Zudem ist die Wahrscheinlichkeit in Universitäten in der Nähe von politischen Machtzentren höher, da dort die Wirksamkeit höher eingeschätzt wird und die Medienpräsenz stärker ist. Eine geringere Wahrscheinlichkeit für Protestereignisse schreiben sie angewandten oder berufsorientierten Universitäten zu, da Studiengänge dort stärker strukturierter sind und Studierende über weniger Freiraum für Protest verfügen. Am meisten beteiligen sich nach den Autor*innen Studierende aus den Sozial- und Humanwissenschaften sowie Mathematiker*innen an Protesten.

\subsubsection{Mobilisierungstheorie}
Nach \citet[532]{jenkinsResourceMobilizationTheory1983} lässt sich Mobilisierung definieren als \textit{\enquote{process by which a group secures collective control over the resources needed for collective action}}. Im Folgenden soll sich vor allem auf die Mobilisierung von Menschen konzentriert werden. \citet{ruchtBedeutungOnlineMobilisierungFuer2014} erklärt diese Art der Mobilisierung durch zwei Stufen:
\begin{enumerate}
	\item \textbf{Konsensusmobilisierung}: Werbung um Zustimmung für Protestanliegen.
	\item \textbf{Aktionsmobilisierung}: Werbung um Protestbeteiligung. Diese Stufe lässt sich nach \citet{vanstekelenburgSocialPsychologyProtest2013} in weitere \linebreak Voraussetzungen unterteilen, die für eine Teilnahme erforderlich sind.
	\begin{enumerate}
		\item Wissen über nächste Termine
		\item Motivation teilzunehmen
		\item Möglichkeit teilzunehmen
	\end{enumerate}
\end{enumerate}
%Arten der Mobilisierung: intern/extern, Instrumente der Mobilisierung, ...
Bei manchen Protesten zeigt sich zudem das Phänomen der Diaspora-\linebreak Mobilisierung. Das bedeutet, dass im Ausland lebende Menschen, die sich zur Gemeinschaft im Protestland zugehörig fühlen, Online-Protest unterstützen und Informationen weiterteilen. Einige wissenschaftliche Veröffentlichungen zeigen, dass Menschen in der Diaspora dadurch den Protest im Protestland beeinflussen. Diaspora-Mobilisierung arbeiten unter anderem \citet{belloEndSARSProtestDiscourse2023} und \citet{olabodeVeteransDiasporaActivism2016} am Beispiel verschiedener Proteste in Nigeria heraus. Auch \citet{monteiroFarmerProtestsIndia2021} betont die wichtige Rolle der Diaspora Unterstützung über Twitter am Beispiel der Proteste indischer Landwirt*innen 2020/21.